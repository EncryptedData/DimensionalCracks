package com.gitlab.EncryptedData.DimensionalCracks.block;

import com.gitlab.EncryptedData.DimensionalCracks.ModDimensionalCracks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.event.RegistryEvent;

public class RegistryBlock {

    private BlockTest blockTest_;
    private ItemBlock itemBlockTest_;

    public RegistryBlock()
    {
        CreateBlocks();
        CreateItemBlocks();
    }

    /***
     * Register all the blocks handled by this instance to Forge
     * @param event Event given by Forge
     */
    public void RegisterBlocks(RegistryEvent.Register<Block> event)
    {
        event.getRegistry().register(blockTest_);

    }

    public void RegisterItemBlocks(RegistryEvent.Register<Item> event)
    {
        event.getRegistry().register(itemBlockTest_);
    }

    /***
     * @return Global instance of BlockTest
     */
    public BlockTest getBlockTest() {
        return blockTest_;
    }

    public ItemBlock getItemBlockTest() {
        return itemBlockTest_;
    }

    private void CreateBlocks()
    {
        blockTest_ = new BlockTest();
    }

    private void CreateItemBlocks()
    {
        itemBlockTest_ = new ItemBlock(blockTest_);
        itemBlockTest_.setUnlocalizedName(blockTest_.getName());
        itemBlockTest_.setRegistryName(ModDimensionalCracks.MODID, blockTest_.getName());
    }
}
