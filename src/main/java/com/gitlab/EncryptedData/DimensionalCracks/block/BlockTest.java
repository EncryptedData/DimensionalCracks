package com.gitlab.EncryptedData.DimensionalCracks.block;

import com.gitlab.EncryptedData.DimensionalCracks.ModDimensionalCracks;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockTest extends Block {

    private static String name_ = "blockTest";
    BlockTest()
    {
        super(Material.ROCK, MapColor.GRAY);
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        this.setUnlocalizedName(name_);
        this.setRegistryName(ModDimensionalCracks.MODID, name_);
    }

    public static String getName() {
        return name_;
    }
}
