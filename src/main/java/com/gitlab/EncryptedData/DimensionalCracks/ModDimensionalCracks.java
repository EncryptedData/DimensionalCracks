package com.gitlab.EncryptedData.DimensionalCracks;

import com.gitlab.EncryptedData.DimensionalCracks.block.RegistryBlock;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
@Mod(modid = ModDimensionalCracks.MODID, version = ModDimensionalCracks.VERSION)
public class ModDimensionalCracks
{
    public static final String MODID = "dimensionalcracks";
    public static final String VERSION = "0.1";

    private static RegistryBlock blockRegistry = new RegistryBlock();

    /***
     * Handle the Block Register Event
     * @param event
     */
    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
    {
        blockRegistry.RegisterBlocks(event);
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
    {
        blockRegistry.RegisterItemBlocks(event);
    }

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event)
    {
        ModelLoader.setCustomModelResourceLocation(blockRegistry.getItemBlockTest(), 0, new ModelResourceLocation(blockRegistry.getItemBlockTest().getRegistryName(), "inventory"));
    }

    @EventHandler
    public void PreInit(FMLInitializationEvent event)
    {

    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    }
}
